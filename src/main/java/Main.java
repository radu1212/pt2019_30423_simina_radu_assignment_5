import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.text.DateFormatSymbols;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

    private ArrayList<MonitoredData> log;

    public Main() throws IOException { }

    public void populateArrayList() {

        this.log = new ArrayList<>();

        String fileName = "c:\\Users\\Radu\\Documents\\Activities.txt";
        File file = new File(fileName);

        try (Stream<String> linesStream = Files.lines(file.toPath())) {
            String start = "";
            String end = "";
            String act = "";
            linesStream.forEach(line -> {
                String a[] = line.split("\t\t");
                MonitoredData aux = new MonitoredData(a[0],a[1],a[2]);
                this.log.add(aux);
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Map<String,Long> countAppearences(){
        return log.stream().collect(Collectors.groupingBy(MonitoredData::getActivity, Collectors.counting()));
    }

    public Map<String,Map<String,Long>> countAppearencesPerDay(){
        return log.stream()
                .collect(Collectors.groupingBy(MonitoredData::getDate,
                        Collectors.groupingBy(MonitoredData::getActivity, Collectors.counting())));
    }

    public int getNoOfDays(){
        Collection<String> list = new ArrayList<>();
        for(MonitoredData m:log){
                String[] a = m.getStartDate().split(" ");
                list.add(a[0]);
        }
        return list.stream().distinct().collect(Collectors.toList()).size();
    }

    public static <K, V> String mapToString(Map<K, V> map) {
        return map.entrySet()
                .stream()
                .map(entry -> entry.getKey() + ":" +entry.getValue() + "\n")
                .collect(Collectors.joining("", "", ""));
    }

    public long findDuration(String start, String end,TimeUnit timeUnit){
        String startTime = start.split(" ")[1];
        String endTime = end.split(" ")[1];

        String startDate = start.split(" ")[0];
        String endDate = end.split(" ")[0];

        String startYear = startDate.split("-")[0];
        String endYear = endDate.split("-")[0];

        String startMonth = startDate.split("-")[1];
        String endMonth = endDate.split("-")[1];

        String startDay = startDate.split("-")[2];
        String endDay = endDate.split("-")[2];

        String startHour = startTime.split(":")[0];
        String endHour = endTime.split(":")[0];

        String startMin = startTime.split(":")[1];
        String endMin = endTime.split(":")[1];

        String startSec = startTime.split(":")[2];
        String endSec = endTime.split(":")[2];

        Date date1 = new Date(Integer.parseInt(startYear), Integer.parseInt(startMonth)-1, Integer.parseInt(startDay), Integer.parseInt(startHour), Integer.parseInt(startMin), Integer.parseInt(startSec));
        Date date2 = new Date(Integer.parseInt(endYear), Integer.parseInt(endMonth)-1, Integer.parseInt(endDay), Integer.parseInt(endHour), Integer.parseInt(endMin), Integer.parseInt(endSec));

        long diffInMillies = date2.getTime() - date1.getTime();
        return timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);
    }

    public void attachDuration(){
        log.forEach((a) -> System.out.println(a + " Activity duration: " + findDuration(a.getStartDate(),a.getEndTime(),TimeUnit.SECONDS)+ " seconds"));
    }

    public Map<String,Integer> findActivityDuration(){
        Map<String, Integer> o =
                log.stream().collect(Collectors.groupingBy(MonitoredData::getActivity,
                        Collectors.summingInt(f1 -> (int) findDuration(f1.getStartDate(),f1.getEndTime(),TimeUnit.SECONDS))));
        System.out.println(mapToString(o));
        return o;
    }

    public boolean checkActivity(String activity){
        int x,y=0;
        x = (int) log.stream().filter(e -> e.getActivity().equals(activity)).filter(e -> findDuration(e.getStartDate(), e.getEndTime(), TimeUnit.MINUTES) < 5).count();
        y = (int) log.stream().filter(e -> e.getActivity().equals(activity)).count();
        float proportion = (float)x / (float)y;
        return proportion > 0.9;
    }

    public List<String> findDistinctActivities(){
        List<String> list = new ArrayList<>();
        log.forEach(e->list.add(e.getActivity()));
        return list.stream().distinct().collect(Collectors.toList());
    }

    public Map<String,Boolean> finalTask(){
        return findDistinctActivities().stream().collect(Collectors.toMap(x->x, this::checkActivity));
    }

    public static void main(String[] args) throws IOException {
        Main main = new Main();
        main.populateArrayList();

        System.out.println("\nThe log contains a number of: " + main.getNoOfDays() + " days\n");

        System.out.println("\n\nBelow you have the number of appearences for each activity\n");
        System.out.println(main.countAppearences());

        System.out.println("\n\nBelow you have the number of appearences for each activity every day\n");
        System.out.println(mapToString(main.countAppearencesPerDay()));

        System.out.println("\n\nBelow you have the duration of each activity\n");
        main.attachDuration();

        System.out.println("\n\nBelow you have the total duration for each activity\n");
        main.findActivityDuration();

        System.out.println("\n\nBelow you have the list of activities that have a proportion of more than 90% with duration under 5 minutes:\n");
        System.out.println(mapToString(main.finalTask()));
    }
}