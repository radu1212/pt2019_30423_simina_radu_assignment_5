import java.util.Date;
import java.util.concurrent.TimeUnit;

public class MonitoredData {
    private String startDate;
    private String endTime;
    private String activity;

    public MonitoredData(String startDate, String endTime, String activity) {
        this.startDate = startDate;
        this.endTime = endTime;
        this.activity = activity;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getDate(){
        return startDate.split(" ")[0];
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    @Override
    public String toString() {
        return "MonitoredData{" +
                "startDate='" + startDate + '\'' +
                ", endTime='" + endTime + '\'' +
                ", activity='" + activity + '\'' +
                '}';
    }
}
